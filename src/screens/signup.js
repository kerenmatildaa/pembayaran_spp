import React from 'react'
import {Text,View,Image, TextInput, TouchableOpacity} from 'react-native'

function SignUp({navigation}){
    return(
        <View style={{margin:20}}> 
            <View style={{ margin: 20 }}>
            <Text style={{fontSize:25, fontWeight:'bold', textAlign:'center', color: '#000000', marginBottom:25}}>Pembayaran SPP</Text>
            <View>
                <Image style={{width: 245, height: 275, marginLeft: 'auto', marginRight: 'auto', marginTop: 20, marginBottom:30 }} source={require('../asset/10.jpg')} />
            </View>
            <View>
                <Text style={{ fontSize: 25, fontWeight: 'bold', marginTop: 20, color: '#000000' }}>
                    Sign Up</Text>
            </View>
            <View style={{marginTop:20}}> 
                    <TextInput
                        placeholder='Nama Lengkap'
                        style={{borderWidth:0,borderRadius:10,padding:10,backgroundColor:'#E5E5E5'}}
                    />
                </View>
                <View style={{marginTop:20}}> 
                    <TextInput
                        placeholder='Username'
                        style={{borderWidth:0,borderRadius:10,padding:10,backgroundColor:'#E5E5E5'}}
                    />
                </View>
                <View style={{marginTop:20}}> 
                    <TextInput
                        placeholder='Password'
                        style={{borderWidth:0,borderRadius:10,padding:10,backgroundColor:'#E5E5E5'}}
                    />
                </View>
            </View>
            <View style={{marginLeft:'auto',marginRight:'auto',marginTop:35}}>
                <TouchableOpacity onPress={()=>navigation.navigate('Home')}>
                    <View style={{width:90,height:30,backgroundColor:'#0F4C81',borderRadius:15}}>
                        <Text style={{marginRight:'auto',marginLeft:'auto',marginBottom:'auto',marginTop:'auto',fontWeight:'bold',color:'#FFFFFF'}}>Sign Up</Text>
                    </View>
                </TouchableOpacity>
            </View>

            <View style={{marginLeft:'auto',marginRight:'auto'}}>
                <TouchableOpacity onPress={()=>navigation.navigate('Login')}>
                    <View style={{width:90,height:30,borderRadius:15}}>
                        <Text style={{marginRight:'auto',marginLeft:'auto',marginBottom:'auto',marginTop:'auto',fontWeight:'bold',color:'#0F4C81'}}>Login</Text>
                    </View>
                </TouchableOpacity>
            </View>
           
        </View>
    )
}

export default SignUp;