import React from 'react'
import {Text,View,Image, TextInput, TouchableOpacity} from 'react-native'

function Home({navigation}){
    return(
        <View>
            <View style={{width:'100%', height:50, backgroundColor:'#B3DAF2'}}></View>
            <View style={{padding:20}}>
                <View style={{flexDirection:'row',marginTop:50}}>
                    <Image style={{height:180, width:180, marginLeft:10}} source={require ('../asset/greeting.png')}/>
                    <Text style={{fontWeight:'bold', color:'black', fontSize:25, marginTop:60,}}>Selamat Datang</Text>
                </View>
                <View style={{backgroundColor:'#E7E7E7',width:'90%',height:200,marginTop:40,marginRight:'auto',marginLeft:'auto',borderRadius:8}}>
                    <Text style={{textAlign:'center', marginTop:15, fontWeight:'bold', }}>Data Siswa</Text>
                </View>
                <View>
                    <View style={{borderRadius:5,width:120,height:30,backgroundColor:'#FC7B7B',justifyContent:'center',alignItems:'center',marginTop:25,}}>
                        <Text style={{fontWeight:'bold'}}>PERLU DIBAYAR</Text>
                    </View> 
                </View>
                <View>
                    <Text style={{fontWeight:'bold', fontSize:15, marginTop:15, marginLeft:15}}>November</Text>
                    <View style={{width:'90%',height:70,backgroundColor:'#E7E7E7',borderRadius:8,padding:10, marginTop:10}}>
                        <Text>Segera Bayar SPP sebesar :</Text>
                        <Text>Rp. 600.000,00</Text>
                        <View style={{flexDirection:'row-reverse'}}>
                            <TouchableOpacity onPress={()=>navigation.navigate('BayarSekarang')}>
                                <Text style={{color:'#FC7B7B',textDecorationLine:'underline'}}>Bayar sekarang</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
       </View>
    )
}

export default Home;