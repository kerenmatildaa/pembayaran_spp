import React from 'react'
import {Text,View,Image, TextInput, TouchableOpacity} from 'react-native'

function BayarSekarang({navigation}){
    return(
        <View>
            <View style={{width:'100%', height:50, backgroundColor:'#B3DAF2'}}>
                <Text style={{marginTop:10, marginLeft:20, fontSize:20, fontWeight:'bold', textAlign:'center'}}>Pembayaran</Text>
            </View>
            <View>
                <View style={{borderRadius:5,width:120,height:30,backgroundColor:'#FC7B7B',justifyContent:'center',alignItems:'center',marginTop:30, marginLeft:25}}>
                    <Text style={{fontWeight:'bold'}}>PERLU DIBAYAR</Text>
                </View> 
            </View>
            <View style={{marginTop:30, flexDirection:'row', padding:15 }}>
                <Image style={{height:40, width:40}} source={require ('../asset/information.png')}/>
                <Text style={{marginLeft: 10, textAlign:'center', fontSize:20, fontWeight:'bold'}}>Harap segera datang ke sekolah untuk melakukan pembayaran</Text>
            </View>
            <View>
            <View style={{backgroundColor:'#B3DAF2',width:'90%',height:200,marginTop:40,marginRight:'auto',marginLeft:'auto',borderRadius:8}}>
                    <View>
                        <Text style={{marginLeft:20, marginTop:15, fontWeight:'bold', fontSize:15}}>Nama    : </Text>
                        <Text style={{marginLeft:20, marginTop:5, fontWeight:'bold', fontSize:15}}>Kelas     : XII</Text>
                        <Text style={{marginLeft:20, marginTop:5,fontWeight:'bold', fontSize:15}}>Jurusan : RPL</Text>
                        <Text style={{marginLeft:20, marginTop:20,fontWeight:'bold', textAlign:'center', fontSize:15}}>Jumlah yang harus di bayarkan sebesar</Text>
                        <Text style={{marginLeft:20, marginTop:25,fontWeight:'bold', textAlign:'center', fontSize:20}}>Rp. 600.000,00</Text>
                    </View>
            </View>
            <View>
                <Image style={{height:276, width:348, marginLeft: 'auto', marginRight:'auto'}} source={require ('../asset/Asset_19.png')}/>
            </View>
            </View>
        </View>
        
        
    )
}

export default BayarSekarang;